print('welcome to tic tac toe ')


def main():
    player1_marker = ''
    player2_marker = ''
    while(player1_marker == ''):
        player1_marker = getMarker('')

    player2_marker = getMarker(player1_marker)
    print(f'player 2 gets = {player2_marker}')

    board = getBoard()
    printBoard(board)

    turn = 'p1'

    while(not isGameOver(board, player1_marker, player2_marker)):
        index = -1
        if(turn == 'p1'):
            while(not isValidInput(index, board)):
                print('Player 1 choose position...')
                index = int(input())
            updateBoard(board,index,player1_marker)
            print(f'player 1 selected {index}')
            turn = 'p2'
        else:
            while(not isValidInput(index, board)):
                print('Player 2 choose position...')
                index = int(input())

            updateBoard(board,index,player2_marker)
            print(f'player 2 selected {index}')
            turn = 'p1'
        printBoard(board)


def isValidInput(index: int, board) -> bool:
    if -1 < index  < 9:
        return board[index] == ''
    else:
       return False

def isGameOver(board, player1_marker, player2_marker)-> bool:
    player1_selections = [i for i, x in enumerate(board) if x == player1_marker]
    player2_selections = [i for i, x in enumerate(board) if x == player2_marker]
    print(player1_selections)
    print(player2_selections)
    if(hasAWin(player1_selections)):
        print("Player 1 won")
        return True;
    elif(hasAWin(player2_selections)):
        print("player 2 won")
        return True
    else:
        return not '' in board


def hasAWin(selections):
    winningCombo = [(0,1,2),(0,3,6), (0,4,8),(1,4,7),(2,4,6),(3,4,5),(6,7,8)]
    for combo in winningCombo:
        comboList = list(combo)
        if any(selections.count(entry) == 0 for entry in comboList):
            continue
        else:
            return True

def getBoard():
    return ['', '' ,'', '' , '', '', '', '', '']

def updateBoard(board, index, symbol):
    board[index] = symbol
    return board

def printBoard(board):
     print(getCell(0, board) + getCell(1, board) + getCell(2, board) )
     print(getCell(3, board) + getCell(4, board) + getCell(5, board) )
     print(getCell(6, board) + getCell(7, board) + getCell(8, board) )

def getCell(index, board):
    val =  board[index]
    if(index in [2,5]):
        if(val == ''):
            return '__'
        else:
            return val + '_'

    elif(index in [6,7]):
         if(val == ''):
            return '  |'
         else:
            return val + ' |'
    elif(index in [0,1,3,4]):
        if(val == ''):
            return '__|'
        else:
            return val + '_|'
    else:
        return val

def getMarker(marker):
    if(marker != 'X' and marker != 'O' ):
        while(marker != 'X' and marker != 'O' ):
            marker =  input(f"Player 1 choose X or O to begin = ", )
            print(f"Player 1 selected = {marker}")
            return marker
    elif(marker == 'X'):
        return 'O'
    else:
        return 'X'

main()
